package com.example.classes
//interface
interface Electricity {
    fun on()
    fun off()
}

class Lamp : Electricity {
    override fun on()
    {
        println("Light is on")
    }

    override fun off()
    {
        println("Light off")
    }
}

fun main()
{
    val obj = Lamp()
    obj.on()
    obj.off()
}