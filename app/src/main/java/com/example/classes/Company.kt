package com.example.classes

//generic

class Company<T> (text : T){
    var x = text
    init{
        println(x)
    }
}
fun main(args: Array<String>){
    var name: Company<String> = Company<String>("Example of using generic")
    var rank: Company<Int> = Company<Int>(2000)
}