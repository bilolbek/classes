package com.example.classes
//
//      Abstarct

abstract class Employee(val name: String,val experience: Int) {
    abstract var salary: Double
    abstract fun dateOfBirth(date:String)

    fun employeeDetails() {
        println("Name of the employee: $name")
        println("Experience in years: $experience")
        println("Annual Salary: $salary")
    }
}
// derived class
class Developer(name: String,experience: Int) : Employee(name,experience) {
    override var salary = 500000.00
    override fun dateOfBirth(date:String){
        println("Date of Birth is: $date")
    }
}
fun main(args: Array<String>) {
    val eng = Developer("Bilolbek",2)
    eng.employeeDetails()
    eng.dateOfBirth("03 January 2000")
}